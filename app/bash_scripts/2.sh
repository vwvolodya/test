#!/usr/bin/env bash

PS3="Select one:"

select WORD in "hello" "world" "from" "bash"
do
    echo "You have chosen ${WORD}"
    if [ ${WORD} == "hello" ]; then
        echo "HI "
        echo $'\a'
    fi
    break
done
