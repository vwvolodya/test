#!/usr/bin/env bash

VAR="Global Var"

function main {
    local VAR="Local Var"
    echo -e ${VAR}
}

echo -e ${VAR}

main

echo -e "=============="

echo uname -a
echo `uname -a`

NEW="uname -a"
NEW1=`uname -a`

echo ${NEW}
echo ${NEW1}
echo -e "hello exec"
echo -e '${NEW}'

echo -e "Please type your name..."
read name

echo -e "You typed ${name} !"

seq 1 10